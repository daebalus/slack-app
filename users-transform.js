const { promises: fs } = require('fs'); // Import fs.promises for async file operations

const REAL_NAME_MAPPING_FILE = 'users-mapped.txt';
const TXT_FILE = 'slack_members.txt';

// Function to extract real names and create a mapping
async function extractRealNames() {
    try {
    // Read members from the text file
    const fileContent = await fs.readFile(TXT_FILE, 'utf-8');
    const members = fileContent.trim().split('\n');

    // Map real names to an array of other real names
    const realNameMapping = {};
    for (const member of members) {
        const match = member.match(/Real Name: (.+)/);
        if (match) {
            const realName = match[1];
            const otherRealNames = members
            .filter((m) => m !== member)
            .map((m) => m.match(/Real Name: (.+)/)[1]);
            realNameMapping[realName] = otherRealNames;
        }
    }

    // Write the mapping to a new text file
    const mappingContent = Object.entries(realNameMapping)
    .map(([realName, otherRealNames]) => `${realName}: [${otherRealNames.join(', ')}]`)
    .join('\n');

    await fs.writeFile(REAL_NAME_MAPPING_FILE, mappingContent);
    console.log(`Real name mapping written to ${REAL_NAME_MAPPING_FILE}`);
} catch (error) {
    console.error(`Error extracting real names: ${error.message}`);
}
}
  
// Execute the function
extractRealNames();