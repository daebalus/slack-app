const { App } = require('@slack/bolt');

const SLACK_BOT_TOKEN = 'xoxb-6542964363602-6545430048676-cRSCnwRvM9NDJdTC24loQkMP';

// File to store user information
const USER_FILE = 'slack_users.txt';

// Initialize Slack App with Socket mode
const app = new App({
  token: SLACK_BOT_TOKEN,
  appToken: process.env.SLACK_APP_TOKEN, // You need to set up an app-level token for socket mode
  socketMode: true,
});

// Event listener for member joined channel
app.event('member_joined_channel', async ({ event, client }) => {
  try {
    // Fetch user information
    const user = await client.users.info({
      user: event.user,
    });

    // Append user information to the text file
    const userData = `ID: ${user.user.id}, Name: ${user.user.name}, Real Name: ${user.user.real_name}`;
    await require('fs').promises.appendFile(USER_FILE, userData + '\n');

    console.log(`User information written to ${USER_FILE}`);
  } catch (error) {
    console.error(`Error fetching user information: ${error.message}`);
  }
});

// Start the app
(async () => {
  await app.start();
  console.log('⚡️ Bolt app is running!');
})();
