const { WebClient } = require('@slack/web-api');
const { promises: fs } = require('fs'); // Import fs.promises for async file operations

// Slack API token
const SLACK_TOKEN = 'xoxb-6542964363602-6545430048676-cRSCnwRvM9NDJdTC24loQkMP';

// ID of the Slack channel you want to fetch members from
const CHANNEL_ID = 'C06G1C9H6VA';

// SQLite database file
// const DB_FILE = 'slack_members.txt';

// Initialize Slack WebClient
const client = new WebClient(SLACK_TOKEN);

const TXT_FILE = 'slack_members.txt';

// Function to fetch and write members to a text file
async function fetchAndWriteMembers() {
    try {
      // Fetch channel members from Slack API
      const response = await client.conversations.members({
        channel: CHANNEL_ID,
      });
      const members = response.members;
  
      // Map members to promises for user info
      const memberDataPromises = members.map(async (memberID) => {
        const userInfo = await client.users.info({
          user: memberID,
        });
        const user = userInfo.user;
        return `ID: ${user.id}, Name: ${user.name}, Real Name: ${user.real_name}`;
      });
  
      // Wait for all promises to resolve
      const memberData = await Promise.all(memberDataPromises);
  
      // Write to the text file
      await fs.appendFile(TXT_FILE, memberData.join('\n') + '\n');
      console.log(`Members written to ${TXT_FILE}`);
    } catch (error) {
      console.error(`Error fetching members: ${error.message}`);
    }
  }
  
  // Execute the function
  fetchAndWriteMembers();